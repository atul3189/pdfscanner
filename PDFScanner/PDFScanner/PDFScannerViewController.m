//
//  ViewController.m
//  IPDFCameraViewController
//
//  Created by Atul M on 11/01/15.
//  Copyright (c) 2015 Atul M. All rights reserved.
//

#import "PDFScannerViewController.h"

#import "IPDFCameraViewController.h"
#import "PreviewViewController.h"
#import "ArrangeCollectionViewController.h"
#import "SegmentContainerViewController.h"
@import AVFoundation;

NSMutableArray *selectedImageArray;

@interface PDFScannerViewController ()<PreviewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet IPDFCameraViewController *cameraViewController;
@property (weak, nonatomic) IBOutlet UIImageView *focusIndicator;
@property (strong, nonatomic) NSString *currentImagePath;
- (IBAction)focusGesture:(id)sender;

- (IBAction)captureButton:(id)sender;

@end

@implementation PDFScannerViewController

#pragma mark -
#pragma mark View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(selectedImageArray == nil) {
        selectedImageArray = [[NSMutableArray alloc]init];
    }
    [self checkCameraPermissions];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveButtonClicked:) name:@"saveButtonPressed" object:nil];
}

- (void)checkCameraPermissions{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        [self.cameraViewController setupCameraView];
        [self.cameraViewController setEnableBorderDetection:YES];
        [self updateTitleLabel];
    } else if(authStatus == AVAuthorizationStatusDenied){
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Camera Error"
                                      message:@"Please provide access to camera from device settings"
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 [self dismissViewControllerAnimated:YES completion:^{
                                 
                                 }];

                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];

    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.cameraViewController setupCameraView];
                    [self.cameraViewController setEnableBorderDetection:YES];
                    [self updateTitleLabel];
                });
                
            } else {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Camera Error"
                                              message:@"Please provide access to camera from device settings"
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Do some thing here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         [self dismissViewControllerAnimated:YES completion:^{
                                             
                                         }];
                                     }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                NSLog(@"Not granted access to AVMediaTypeVideo");
            }
        }];
    } else {
        // impossible, unknown authorization status
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.cameraViewController start];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)saveButtonClicked:(NSNotification*)notification{
    [self createPDFWithImagesArray:selectedImageArray andFileName:@"savedDocument"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *PDFPath = [documentsDirectory stringByAppendingPathComponent:@"savedDocument.pdf"];
    NSData *pdfData = [NSData dataWithContentsOfFile:PDFPath];
    if(_delegate && [_delegate respondsToSelector:@selector(scanner: didCapturePdfWithData:)]){
        [_delegate scanner:self didCapturePdfWithData:pdfData];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        selectedImageArray = [NSMutableArray array];
    }];
}

+ (void)addImage:(UIImage*)image{
    if(image != nil){
        [selectedImageArray addObject:image];
    }
}

+ (int)numberOfImages{
    return (int)selectedImageArray.count;
}

+ (void)replaceImageAtIndex:(int)index1 withImage:(UIImage*)image{
    [selectedImageArray replaceObjectAtIndex:index1 withObject:image];
}

+ (NSMutableArray*)imageList{
    return [selectedImageArray mutableCopy];
//    return selectedImageArray;
}

+ (UIImage*)imageAtIndex:(int)index{
    if(selectedImageArray.count > index){
        return [selectedImageArray objectAtIndex:index];
    }
    return nil;
}

+ (void)setImageList:(NSMutableArray*)imageList{
    selectedImageArray = [imageList mutableCopy];
//    selectedImageArray = imageList;
}

+ (void)removeImageAtIndex:(int)index{
    if(selectedImageArray.count > index){
        [selectedImageArray removeObjectAtIndex:index];
    }
}

+ (void)removeAllImages{
    [selectedImageArray removeAllObjects];
}

#pragma mark -
#pragma mark CameraVC Actions

- (IBAction)focusGesture:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint location = [sender locationInView:self.cameraViewController];
        
        [self focusIndicatorAnimateToPoint:location];
        
        [self.cameraViewController focusAtPoint:location completionHandler:^
         {
             [self focusIndicatorAnimateToPoint:location];
         }];
    }
}

- (void)focusIndicatorAnimateToPoint:(CGPoint)targetPoint
{
    [self.focusIndicator setCenter:targetPoint];
    self.focusIndicator.alpha = 0.0;
    self.focusIndicator.hidden = NO;
    
    [UIView animateWithDuration:0.4 animations:^
    {
         self.focusIndicator.alpha = 1.0;
    }
    completion:^(BOOL finished)
    {
         [UIView animateWithDuration:0.4 animations:^
         {
             self.focusIndicator.alpha = 0.0;
         }];
     }];
}

- (IBAction)borderDetectToggle:(id)sender
{
    BOOL enable = !self.cameraViewController.isBorderDetectionEnabled;
    [self changeButton:sender targetTitle:(enable) ? @"CROP On" : @"CROP Off" toStateEnabled:enable];
    self.cameraViewController.enableBorderDetection = enable;
    [self updateTitleLabel];
}

- (IBAction)filterToggle:(id)sender
{
    [self.cameraViewController setCameraViewType:(self.cameraViewController.cameraViewType == IPDFCameraViewTypeBlackAndWhite) ? IPDFCameraViewTypeNormal : IPDFCameraViewTypeBlackAndWhite];
    [self updateTitleLabel];
}

- (IBAction)torchToggle:(id)sender
{
    BOOL enable = !self.cameraViewController.isTorchEnabled;
    [self changeButton:sender targetTitle:(enable) ? @"FLASH On" : @"FLASH Off" toStateEnabled:enable];
    self.cameraViewController.enableTorch = enable;
}

- (void)updateTitleLabel
{
    CATransition *animation = [CATransition animation];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromBottom;
    animation.duration = 0.35;
    [self.titleLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
    
    NSString *filterMode = (self.cameraViewController.cameraViewType == IPDFCameraViewTypeBlackAndWhite) ? @"TEXT FILTER" : @"COLOR FILTER";
    self.titleLabel.text = [filterMode stringByAppendingFormat:@" | %@",(self.cameraViewController.isBorderDetectionEnabled)?@"AUTOCROP On":@"AUTOCROP Off"];
}

- (void)changeButton:(UIButton *)button targetTitle:(NSString *)title toStateEnabled:(BOOL)enabled
{
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:(enabled) ? [UIColor colorWithRed:1 green:0.81 blue:0 alpha:1] : [UIColor whiteColor] forState:UIControlStateNormal];
}


#pragma mark -
#pragma mark CameraVC Capture Image

- (IBAction)captureButton:(id)sender
{
    __weak typeof(self) weakSelf = self;
    [self.cameraViewController captureImageWithCompletionHander:^(NSString *imageFilePath)
    {
        weakSelf.currentImagePath = imageFilePath;
        [weakSelf performSegueWithIdentifier:@"showSegmentContainer" sender:self];
    }];
}

# pragma mark - PDF creation

- (void)createPDFWithImagesArray:(NSMutableArray *)array andFileName:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *PDFPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",fileName]];
    
    UIGraphicsBeginPDFContextToFile(PDFPath, CGRectZero, nil);
    for (UIImage *image in array)
    {
        // Mark the beginning of a new page.
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, image.size.width, image.size.height), nil);
        
        [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    }
    UIGraphicsEndPDFContext();
}

# pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showSegmentContainer"]){
        SegmentContainerViewController *segmentVC = segue.destinationViewController;
        segmentVC.imageIndex = (int) selectedImageArray.count;
        [PDFScannerViewController addImage:[UIImage imageWithContentsOfFile:self.currentImagePath]];
        segmentVC.previewImagePath = self.currentImagePath;
    }
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if([segue.identifier isEqualToString:@"showPreview"]){
//        PreviewViewController *preview = segue.destinationViewController;
//        preview.delegate = self;
//        preview.imageIndex = (int) selectedImageArray.count;
//        preview.previewImagePath = self.currentImagePath;
//    } else if([segue.identifier isEqualToString:@"showArrange"]){
//    }
//}
//
//-(void)previewControllerNavigateToArrangeView:(PreviewViewController *)previewController{
//    [self.navigationController setNavigationBarHidden:YES];
//    [self.navigationController popToRootViewControllerAnimated:NO];
//    [self performSegueWithIdentifier:@"showArrange" sender:self];
//}

@end
