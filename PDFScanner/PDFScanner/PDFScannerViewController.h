//
//  ViewController.h
//  IPDFCameraViewController
//
//  Created by Atul M on 11/01/15.
//  Copyright (c) 2015 Atul M. All rights reserved.
//

@class PDFScannerViewController;
#import <UIKit/UIKit.h>

@protocol PDFScannerViewDelegate <NSObject>

- (void)scanner:(PDFScannerViewController*)scanner didCapturePdfWithData:(NSData*)data;

@end

@interface PDFScannerViewController : UIViewController

@property (nonatomic, weak) id<PDFScannerViewDelegate> delegate;

+ (void)addImage:(UIImage*)image;
+ (int)numberOfImages;
+ (void)replaceImageAtIndex:(int)index1 withImage:(UIImage*)image;
+ (NSMutableArray*)imageList;
+ (UIImage*)imageAtIndex:(int)index;
+ (void)setImageList:(NSMutableArray*)imageList;
+ (void)removeImageAtIndex:(int)index;
+ (void)removeAllImages;
@end

