//
//  SegmentContainerViewController.m
//  PDFScanner
//
//  Created by Atul M on 19/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import "SegmentContainerViewController.h"
#import "PreviewViewController.h"
#import "ArrangeCollectionViewController.h"
#import "PDFScannerViewController.h"

@interface SegmentContainerViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) UIViewController *currentViewController;

@end

@implementation SegmentContainerViewController

- (void)viewDidLoad {
    self.currentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviewViewController"];
    
    PreviewViewController *preview = (PreviewViewController*)self.currentViewController;
    preview.delegate = self;
    preview.imageIndex = [PDFScannerViewController numberOfImages]-1;
    preview.previewImagePath = self.previewImagePath;
    
    self.currentViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addChildViewController:self.currentViewController];
    [self addSubview:self.currentViewController.view toView:self.containerView];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (IBAction)didChangeSegmentControl:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl*)sender;
    if (segmentedControl.selectedSegmentIndex == 0) {
        
        if([PDFScannerViewController numberOfImages] == 0){
            [self didClickCancelButton:nil];
            return;
        }
        PreviewViewController *preview = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviewViewController"];
        preview.view.translatesAutoresizingMaskIntoConstraints = NO;
        preview.delegate = self;
        preview.imageIndex = (int) [PDFScannerViewController numberOfImages]-1;
        preview.previewImagePath = self.previewImagePath;
        [self cycleFromViewController:self.currentViewController toViewController:preview];
        self.currentViewController = preview;
    } else {
        
        PreviewViewController *preview = (PreviewViewController*)self.currentViewController;
        [preview addOrReplaceImageAtCurrentIndex];
        
        ArrangeCollectionViewController *newViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArrangeCollectionViewController"];
        newViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self cycleFromViewController:self.currentViewController toViewController:newViewController];
        self.currentViewController = newViewController;
    }
}

- (IBAction)didClickCancelButton:(id)sender{
    [PDFScannerViewController removeAllImages];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickSaveButton:(id)sender{
        [[NSNotificationCenter defaultCenter]postNotificationName:@"saveButtonPressed" object:nil];
}

- (void)cycleFromViewController:(UIViewController*) oldViewController
               toViewController:(UIViewController*) newViewController {
    [oldViewController willMoveToParentViewController:nil];
    [self addChildViewController:newViewController];
    [self addSubview:newViewController.view toView:self.containerView];
    newViewController.view.alpha = 0;
    [newViewController.view layoutIfNeeded];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         newViewController.view.alpha = 1;
                         oldViewController.view.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [oldViewController.view removeFromSuperview];
                         [oldViewController removeFromParentViewController];
                         [newViewController didMoveToParentViewController:self];
                     }];
}

- (void)addSubview:(UIView *)subView toView:(UIView*)parentView {
    [parentView addSubview:subView];
    
    NSDictionary * views = @{@"subView" : subView,};
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subView]|"
                                                                   options:0
                                                                   metrics:0
                                                                     views:views];
    [parentView addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subView]|"
                                                          options:0
                                                          metrics:0
                                                            views:views];
    [parentView addConstraints:constraints];
}

# pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showPreview"]){
        PreviewViewController *preview = segue.destinationViewController;
        preview.delegate = self;
        preview.imageIndex = (int) self.imageIndex;
        preview.previewImagePath = self.previewImagePath;
    } else if([segue.identifier isEqualToString:@"showArrange"]){
        
    }
}


@end
