//
//  ArrangeCollectionViewController.m
//  PDFScanner
//
//  Created by Atul M on 10/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import "ArrangeCollectionViewController.h"
#import "PDFScannerViewController.h"
@interface ArrangeCollectionViewController ()

@end

@implementation ArrangeCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    self.collectionView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    self.imageList = [PDFScannerViewController imageList];
    self.navigationController.navigationBarHidden = NO;
        
    UIBarButtonItem *saveBarButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonPressed)];
    [self.navigationItem setRightBarButtonItems:@[saveBarButton]];
    
    // Do any additional setup after loading the view.
}

- (void)saveButtonPressed{
    [PDFScannerViewController setImageList:self.imageList];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"saveButtonPressed" object:nil];
}

- (IBAction)deleteButtonPressed:(id)sender{
    UIButton *deleteButton = (UIButton*)sender;
    UICollectionViewCell *cell = (UICollectionViewCell*)[[deleteButton superview]superview];
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    [self removeItemAtIndex:(int)indexPath.row];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//-(void)willMoveToParentViewController:(UIViewController *)parent {
//    [super willMoveToParentViewController:parent];
//    if (!parent){
//        [PDFScannerViewController setImageList:self.imageList];
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.imageList.count == 0){
        self.navigationItem.rightBarButtonItem.enabled = NO;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    return self.imageList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150, 150)];
    view.image = [self.imageList objectAtIndex:indexPath.item];
    [cell.contentView addSubview:view];
    // Configure the cell
    
    UIImage *deleteImage = [UIImage imageNamed:@"cancel" inBundle:[NSBundle bundleForClass:[ArrangeCollectionViewController class]] compatibleWithTraitCollection:nil];
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.tag = indexPath.row;
    [deleteButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    deleteButton.frame = CGRectMake( 150-32, 0, 32,32 );
    [deleteButton setImage:deleteImage forState:UIControlStateNormal];
    [cell.contentView addSubview:deleteButton];
    
    return cell;
}

-(void)removeItemAtIndex:(int)i {
    [self.collectionView performBatchUpdates:^{
        [self.imageList removeObjectAtIndex:i];
        [PDFScannerViewController setImageList:self.imageList];
        NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
        [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    UIImage *movedImage = [self.imageList objectAtIndex:sourceIndexPath.row];
    [self.imageList removeObject:movedImage];
    if (sourceIndexPath.row > destinationIndexPath.row) {
        //Item is moved backwards
        [self.imageList insertObject:movedImage atIndex:destinationIndexPath.row];
        
    }else{
        //Item is moved forwards
        [self.imageList insertObject:movedImage atIndex:destinationIndexPath.row-1];
    }
    
    
//    [self.imageList exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    [PDFScannerViewController setImageList:self.imageList];
}

@end
