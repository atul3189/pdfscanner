//
//  ArrangeCollectionViewController.h
//  PDFScanner
//
//  Created by Atul M on 10/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ArrangeCollectionViewController;
@protocol ArrangeDelegate <NSObject>

@end

@interface ArrangeCollectionViewController : UICollectionViewController
@property (nonatomic, strong) NSMutableArray *imageList;
@property (nonatomic, weak) id<ArrangeDelegate> delegate;
@end
