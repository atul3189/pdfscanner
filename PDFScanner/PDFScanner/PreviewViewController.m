//
//  PreviewViewController.m
//  PDFScanner
//
//  Created by Atul M on 10/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import "PreviewViewController.h"
#import "PDFScannerViewController.h"
#import "ArrangeCollectionViewController.h"

@interface UIImage (RotationMethods)
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
@end

@implementation UIImage (RotationMethods)

static CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.size.width, self.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

@end



@interface PreviewViewController ()<ArrangeDelegate>
- (IBAction)rotateButtonTapped:(id)sender;

@end

@implementation PreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self addGestureRecognisers];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.previewImage.image = [PDFScannerViewController imageAtIndex:_imageIndex];
    [self addGestureRecognisers];

}

- (void)addGestureRecognisers{
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(leftSwipe)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipe)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:leftSwipe];
    [self.view addGestureRecognizer:rightSwipe];
}

- (void)leftSwipe{
    if(_imageIndex + 1 < [PDFScannerViewController numberOfImages]){
        [self addOrReplaceImageAtCurrentIndex];
        _imageIndex ++;
        self.previewImage.image = [PDFScannerViewController imageAtIndex:_imageIndex];
    }
}

- (void)rightSwipe{
    if(_imageIndex - 1 >= 0){
        [self addOrReplaceImageAtCurrentIndex];
        _imageIndex--;
        self.previewImage.image = [PDFScannerViewController imageAtIndex:_imageIndex];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)rotateButtonTapped:(id)sender{
    self.previewImage.image = [self.previewImage.image imageRotatedByDegrees:90];
}

- (IBAction)addNewPageTapped:(id)sender{
    [self addOrReplaceImageAtCurrentIndex];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)discardPageTapped:(id)sender{
    [PDFScannerViewController removeImageAtIndex:_imageIndex];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)addOrReplaceImageAtCurrentIndex{
    if([PDFScannerViewController numberOfImages] > _imageIndex){
        [PDFScannerViewController replaceImageAtIndex:_imageIndex withImage:self.previewImage.image];
    }else {
        [PDFScannerViewController addImage:self.previewImage.image];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showAddNewPage"]){
        [self addOrReplaceImageAtCurrentIndex];
    }
}


@end

