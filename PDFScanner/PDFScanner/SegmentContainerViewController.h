//
//  SegmentContainerViewController.h
//  PDFScanner
//
//  Created by Atul M on 19/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegmentContainerViewController : UIViewController

@property (nonatomic, strong) NSString *previewImagePath;
@property (nonatomic, assign) int imageIndex;

@end
