//
//  PreviewViewController.h
//  PDFScanner
//
//  Created by Atul M on 10/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PreviewViewController;

@protocol PreviewDelegate <NSObject>

- (void)previewControllerNavigateToArrangeView:(PreviewViewController*)previewController;

@end

@interface PreviewViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *previewImage;
@property (nonatomic, strong) NSString *previewImagePath;
@property (nonatomic, assign) int imageIndex;
@property (nonatomic, weak) id<PreviewDelegate> delegate;

- (void)addOrReplaceImageAtCurrentIndex;
@end
