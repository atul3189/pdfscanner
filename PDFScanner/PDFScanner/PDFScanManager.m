//
//  PDFScanManager.m
//  PDFScanner
//
//  Created by Atul M on 13/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import "PDFScanManager.h"
#import <UIKit/UIKit.h>
#import "PDFScannerViewController.h"
@import AVFoundation;

static PDFScanManager *sharedManager;

@interface PDFScanManager ()<PDFScannerViewDelegate>

@end

@implementation PDFScanManager

+(PDFScanManager*)sharedPDFScanManager
{
    @synchronized([PDFScanManager class])
    {
        if (!sharedManager)
            sharedManager = [[self alloc] init];
        return sharedManager;
    }
    return nil;
}


- (void)presentPDFScanner{
    [self checkCameraPermissions];
}

- (void)loadCamera{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle bundleForClass:[PDFScanManager class]]];
    UINavigationController *navController = [storyBoard instantiateViewControllerWithIdentifier:@"scannerController"];
    PDFScannerViewController *scannerVc = (PDFScannerViewController*)[navController topViewController];
    [[[[UIApplication sharedApplication]keyWindow]rootViewController] presentViewController:navController animated:YES completion:^{
        scannerVc.delegate = [PDFScanManager sharedPDFScanManager];
    }];
}

- (void)checkCameraPermissions{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        [self loadCamera];
    } else if(authStatus == AVAuthorizationStatusDenied){
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Camera Error"
                                      message:@"Please provide access to camera from device settings"
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here
                                 [alert dismissViewControllerAnimated:YES completion:nil];

                                 
                             }];
        [alert addAction:ok];
        [[[[UIApplication sharedApplication]keyWindow]rootViewController] presentViewController:alert animated:YES completion:nil];
        
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self loadCamera];
                });
                
            } else {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Camera Error"
                                              message:@"Please provide access to camera from device settings"
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Do some thing here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alert addAction:ok];
                [[[[UIApplication sharedApplication]keyWindow]rootViewController] presentViewController:alert animated:YES completion:nil];
                NSLog(@"Not granted access to AVMediaTypeVideo");
            }
        }];
    } else {
        // impossible, unknown authorization status
    }
}


- (void)scanner:(PDFScannerViewController *)scanner didCapturePdfWithData:(NSData *)data{
    if(_delegate && [_delegate respondsToSelector:@selector(scanner:didCapturePdfWithData:)]){
        [_delegate scanner:self didCapturePdfWithData:data];
    }
}
@end
