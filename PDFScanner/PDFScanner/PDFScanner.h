//
//  PDFScanner.h
//  PDFScanner
//
//  Created by Atul M on 13/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PDFScanner.
FOUNDATION_EXPORT double PDFScannerVersionNumber;

//! Project version string for PDFScanner.
FOUNDATION_EXPORT const unsigned char PDFScannerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PDFScanner/PublicHeader.h>

#import <PDFScanner/PDFScanManager.h>
