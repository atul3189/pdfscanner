//
//  PDFScanManager.h
//  PDFScanner
//
//  Created by Atul M on 13/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PDFScanManager;
@protocol PDFScannerDelegate <NSObject>

- (void)scanner:(PDFScanManager*)scanner didCapturePdfWithData:(NSData*)data;

@end

@interface PDFScanManager : NSObject

@property (nonatomic, weak) id<PDFScannerDelegate> delegate;

+(PDFScanManager*)sharedPDFScanManager;
- (void)presentPDFScanner;

@end
