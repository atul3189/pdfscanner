//
//  main.m
//  PDFTestApp
//
//  Created by Atul M on 13/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
