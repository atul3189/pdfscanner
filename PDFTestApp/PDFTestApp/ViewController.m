//
//  ViewController.m
//  PDFTestApp
//
//  Created by Atul M on 13/08/16.
//  Copyright © 2016 Atul M. All rights reserved.
//

#import "ViewController.h"
@import PDFScanner;
@interface ViewController ()<PDFScannerDelegate, UIDocumentInteractionControllerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (IBAction)didClickOnButton:(id)sender {
    [PDFScanManager sharedPDFScanManager].delegate = self;
    [[PDFScanManager sharedPDFScanManager] presentPDFScanner];
}

- (void)scanner:(PDFScanManager *)scanner didCapturePdfWithData:(NSData *)data{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"FileName.pdf"];
    [data writeToFile:dataPath atomically:YES];
    
    
    UIDocumentInteractionController *docInteraction = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:dataPath]];
    docInteraction.delegate = self;
    [docInteraction performSelector:@selector(presentPreviewAnimated:) withObject:[NSNumber numberWithBool:YES] afterDelay:1.0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}
@end
